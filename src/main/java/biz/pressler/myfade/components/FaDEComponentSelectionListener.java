package biz.pressler.myfade.components;

public interface FaDEComponentSelectionListener {
	
	void selected(FaDEComponent fc);

}
