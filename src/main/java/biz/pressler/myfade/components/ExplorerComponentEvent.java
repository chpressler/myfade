package biz.pressler.myfade.components;

import java.util.EventObject;

public class ExplorerComponentEvent extends EventObject {

	private static final long serialVersionUID = 1L;

	public ExplorerComponentEvent(Object source) {
		super(source);
	}
	
}
