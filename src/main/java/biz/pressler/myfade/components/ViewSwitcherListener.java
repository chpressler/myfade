package biz.pressler.myfade.components;

public interface ViewSwitcherListener {
	
	void viewChanged(IExplorerComponent iec);

}
